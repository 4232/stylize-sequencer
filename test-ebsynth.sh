#!/bin/bash

# download example
wget -c https://download.tuxfamily.org/4232/wordpress/2017/05/4232.cf-e2.gif

# extras image
ffmpeg -n -i 4232.cf-e2.gif w.%04d.png

#create map

convert \( xc:#000000 xc:#FF0000 +append \) \( xc:#00FF00 xc:#FFFF00 +append \) -append -size 500x281 xc:white +swap -fx 'v.p{i/(w-1),j/(h-1)}' map.png

# gmic
gmic  w.0001.png  _fx_stylize starrynight +fx_stylize 1,6,0,0,0.5,2,3,0.5,0.1,3,3,0,0.7,1,0,1,0,5,5,7,1,30,1,2,1.85,0 output[2] result_w.0001.png

# ebsynth
ls w.*png | while read A; do
	ebsynth -style result_w.0001.png -guide w.0001.png $A -weight 2 -guide map.png map.png -weight 1 -output eb_$A
done

ffmpeg -y -i eb_w.%04d.png -r 4 eb_result.mkv
