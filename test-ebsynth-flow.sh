#!/bin/bash

# download example
wget -c https://download.tuxfamily.org/4232/wordpress/2017/05/4232.cf-e2.gif

# extras image
ffmpeg -n -i 4232.cf-e2.gif w.%04d.png

#create map

convert \( xc:#000000 xc:#FF0000 +append \) \( xc:#00FF00 xc:#FFFF00 +append \) -append -size 500x281 xc:white +swap -fx 'v.p{i/(w-1),j/(h-1)}' map.png

# gmic
gmic  w.0001.png  _fx_stylize starrynight +fx_stylize 1,6,0,0,0.5,2,3,0.5,0.1,3,3,0,0.7,1,0,1,0,5,5,7,1,30,1,2,1.85,0 output[2] result_w.0001.png

bilateral_filter w.0001.png b_w.0001.png 0.01 128 16
convert b_w.0001.png -alpha remove PNG24:b_w.0001.png

# ebsynth
ls w.*png | while read A; do
	# bilateral filter
	bilateral_filter $A b_$A 0.01 128 16
	convert b_$A -alpha remove PNG24:b_$A
	# flow to images in 24bits (deepflow2 dont support 32bits)
	deepflow2 b_w.0001.png b_$A map.flo -d .4 -iter 5
	# map warp
	color_flow_map map.flo warp.png 1
	# new image
	ebsynth -uniformity 0 -style result_w.0001.png -guide b_w.0001.png  b_$A -weight 500  -guide warp.png map.png -weight 1000  -output ebf_$A
done

ffmpeg -y -i ebf_w.%04d.png -r 4 ebf_result.mkv
