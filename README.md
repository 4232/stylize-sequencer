Example from work done on [4232](https://4232.cf)

# Stylizer video

![original](https://download.tuxfamily.org/4232/wordpress/2017/05/4232.cf-e2.gif)

### Download

```
wget https://gitlab.com/4232/stylize-sequencer/-/archive/master/stylize-sequencer-master.zip
unzip stylize-sequencer-master.zip
cd stylize-sequencer-master
```


## Example of G'MIC stylizer video sequencer


### Test

```
chmod +x test-gmic.sh  
./test-gmic.sh
```


### Result

![](https://cf.mastohost.com/v1/AUTH_91eb37814936490c95da7b85993cc2ff/mographsocial/media_attachments/files/105/182/636/202/382/152/original/bb7ce4afdf935d91.mp4)

## Example of G'MIC stylizer + ebsynth


### Test

```
chmod +x test-ebsynth.sh  
./test-ebsynth.sh
```


### Result

![](https://cf.mastohost.com/v1/AUTH_91eb37814936490c95da7b85993cc2ff/mographsocial/media_attachments/files/105/183/881/309/541/157/original/c93dc4bb224f14b7.mp4)


## Example of G'MIC stylizer + ebsynth + flow


### Test

```
chmod +x test-ebsynth-flow.sh  
./test-ebsynth-flow.sh
```


### Result

![](https://cf.mastohost.com/v1/AUTH_91eb37814936490c95da7b85993cc2ff/mographsocial/media_attachments/files/105/350/979/876/309/963/original/7b8050975c60ddd8.mp4)


