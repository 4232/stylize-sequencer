#!/bin/bash

# download example
wget -c https://download.tuxfamily.org/4232/wordpress/2017/05/4232.cf-e2.gif

# extras image
ffmpeg -i 4232.cf-e2.gif w.%04d.png

# gmic
ls w.*png | while read A; do
	gmic  $A  _fx_stylize starrynight +fx_stylize 1,6,0,0,0.5,2,3,0.5,0.1,3,3,0,0.7,1,0,1,0,5,5,7,1,30,1,2,1.85,0 output[2] result_$A
done

ffmpeg -y -i result_w.%04d.png result.mkv
